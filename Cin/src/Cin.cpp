//============================================================================
// Name        : Cin.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int number;

	cout << "Enter a decimal number: ";
	cin >> number;

	cout << "value in octal = 0" << oct << number << endl;
	cout << "value in hex = 0x" << hex << number << endl;

	return 0;
}
