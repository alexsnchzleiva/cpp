//============================================================================
// Name        : EntradaSalida.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>

using namespace std;


int main() {

	ifstream in("texto1.txt");
	ofstream out("texto2.txt");

	string s, texto;

	while(getline(in,s)){
		out << s << "\n";
		texto += s;
	}

	cout << texto << endl ;

	return 0;
}
