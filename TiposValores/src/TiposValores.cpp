//============================================================================
// Name        : TiposValores.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	char c;
	unsigned char cu;
	int i;
	unsigned int iu;
	short int is;
	short iis;
	long int il;
	long iil;
	float f;
	double d;
	long double ld;

	cout << sizeof(c)
		 << "\n"
		 << sizeof(cu) << endl;

	return 0;
}
