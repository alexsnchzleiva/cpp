//============================================================================
// Name        : ConcatenarString.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {
	cout << "This is far too long to put on a "
	    "single line but\n it can be broken up with "
	    "no ill effects\nas long as there is no "
	    "punctuation separating adjacent character "
	    "arrays.\n";
	return 0;
}
