//============================================================================
// Name        : Punteros.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;



int main() {

	int i = 47;
	int* ipa = &i;

	cout << "Direccion de memoria: " << (long)ipa << " = " << i << endl;

	*ipa = 49;

	cout << "Direccion de memoria: " << (long)ipa << " = " << i << endl;

	return 0;
}
