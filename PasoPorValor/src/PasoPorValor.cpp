//============================================================================
// Name        : PasoPorValor.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

void funcion(int a){
	a=5;
	cout << a << endl;
}

int main() {

	int i=4;
	cout << i << endl;

	funcion(i);
	cout << i << endl;

	return 0;
}
