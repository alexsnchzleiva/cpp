//============================================================================
// Name        : ifElse.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

int main() {

	int valor=3;

	if(valor>4){
		cout << "IF" << endl;
	}
	else if(valor<4){
		cout << "ELSE IF" << endl;
	}
	else{
		cout << "ELSE" << endl;
	}

	return 0;
}
