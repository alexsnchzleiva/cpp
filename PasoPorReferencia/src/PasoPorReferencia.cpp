//============================================================================
// Name        : PasoPorReferencia.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;


void pasoRef(int* i){
	*i = 5;
	cout << *i << endl;
}


int main() {

	int a=4;

	cout << a << endl;

	pasoRef(&a);

	cout << a << endl;

	return 0;
}
