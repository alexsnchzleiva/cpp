//============================================================================
// Name        : Return.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>

using namespace std;

char cfunc(int i){

	if(i==0){
		return 'a';
	}
	if(i==1){
		return 'b';
	}
	return 'c';
}


int main() {

	int numero;
	char valor;

	cout << "Introduce un número:\n" << endl;
	cin >> numero;
	valor = cfunc(numero);

	cout << valor << endl;

	return 0;
}
