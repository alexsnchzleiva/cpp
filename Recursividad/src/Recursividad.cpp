//============================================================================
// Name        : Recursividad.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;


void removeHat(char cat){

	for(char c='A';c<cat;c++){
		cout << " ";
	}

	if(cat <= 'Z'){
		cout << "cat" << cat << endl;
		removeHat(cat +1);
	}
	else{
		cout << "Voom!!" << endl;
	}

}

int main() {

	removeHat('A');

	return 0;
}
